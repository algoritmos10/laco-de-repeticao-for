/* console.log("Laço de Repetição FOR")
for(let i=1;i<11;i++){
console.log("Número "+ i)
} */

console.log("EXERCÍCIO 1")
console.log("A)")

for(let i=1; i<101; i++){
    console.log("Número "+ i)
}

console.log(".")

console.log("B)")
for(let i=10; i<51; i+=2){
    console.log("Número "+i)
}

console.log(".")

console.log("C)")
for(let i=11; i<51; i+=2){
    console.log("Número "+i)
}

console.log(".")

console.log("D)")
for(let i=20; i>=0; i--){
    console.log("Número "+i)
}

console.log(".")



console.log("EXERCÍCIO 2")
function soma3a10(){
    let soma = 0
    for(let i=3; i<11; i++){
        soma+=i
    }
return soma
}
console.log("A) "+soma3a10())

function soma_pares4a20(){
    let soma = 0
    for(let i=4; i<21; i+=2){

        soma+=i
    }
    return soma
}
console.log("B) "+soma_pares4a20())

function soma_impares10a35(){
    let soma = 0
    for(let i=11; i<=35; i+=2){
        soma+=i
    }
    return soma
}
console.log("C) "+soma_impares10a35())

console.log(".")



console.log("EXERCÍCIO 3")
function mult2a9(){
    let mult = 1
    for(let i=2; i<=9; i++){
        mult*=i
    }
    return mult
}
console.log(mult2a9())

console.log(".")



console.log("EXECÍCIO 4")
function tabuada_do_n(n){
    for(let i=1; i<11; i++){
        console.log(n+" X "+i+" = "+ n*i)
    }
}
tabuada_do_n(19)

console.log(".")


console.log("EXERCÍCIO 5")
/* let resposta = prompt("Qual valor para a tabuada?")
let número = Number (resposta)
console.log(typeof número)
tabuada_do_n(número) */
function tabuada1a9(){
    for(let i=1; i<10; i++){
        console.log("Tabuada do "+i)
        tabuada_do_n(i)
        console.log(".")
    }
}
tabuada1a9()